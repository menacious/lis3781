> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Daniel Meneses

### Assignment 3 Requirements:

*Three Parts:*

1. Questions
2. Logging into Oracle SQL Server using RemoteLabs
3. Using Oracle SQL Developer to create and populate tables

#### README.md file should include the following items:

* Screenshot of A3 SQL code inside Oracle environment
* Screenshot of populated tables

#### Assignment Screenshots:

*Screenshot of A3 SQL code (part 1)*

![A3 SQL Code Screenshot](img/lis3781_a3_sql_code_a.png)

*Screenshot of A3 SQL code (part 2)*

![A3 SQL Code Screenshot](img/lis3781_a3_sql_code_b.png)

*Screenshots of A3 Populated Tables*

![A3 SQL Populated Tables Screenshot A](img/a3_oracle_data_a.png)

![A3 SQL Populated Tables Screenshot B](img/a3_oracle_data_b.png)

![A3 SQL Populated Tables Screenshot C](img/a3_oracle_data_c.png)

*Bitbucket Repo Link*

[Main LIS3781 Repo Link](https://bitbucket.org/menacious/lis3781/src/master/)
> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Daniel Meneses

### Assignment 4 Requirements:

*Three Parts:*

1. Questions
2. Performing data obfuscation (i.e. salting and hashing) using Microsoft SQL Server
3. Creating an Entity Relationship Diagram (ERD) within MS SQL Server

#### README.md file should include the following items:

* Screenshot of A4 ERD from MS SQL Server
* Screenshot of SQL code used to populate tables
* Main Bitbucket repo link

#### Assignment Screenshots:

*Screenshot of A4 ERD*:

![A4 ERD Screenshot](img/a4_erd.png)

*Screen Recording of A4 SQL code*:

![A4 SQL Code Screen Recording](img/lis3781_a4_sql_code.gif)

*Bitbucket Repo Link*:

[Main LIS3781 Repo Link](https://bitbucket.org/menacious/lis3781/src/master/)
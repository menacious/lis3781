> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Daniel Meneses

### Assignment 2 Requirements:

*Three Parts:*

1. Questions
2. Creating tables using MySQL
3. Creating users and grants

#### README.md file should include the following items:

* Screenshot of A2 SQL code
* Screenshot of populated tables

#### Assignment Screenshots:

*Screenshot of A2 SQL code (part 1)

![A2 SQL Code Screenshot](img/lis3781_a2_sql_code_a.png)

*Screenshot of A2 SQL code (part 2)

![A2 SQL Code Screenshot](img/lis3781_a2_sql_code_b.png)

*Screenshot of A2 Populated Tables

![A2 SQL Populated Tables Screenshot](img/lis3781_a2_pop_tables.png)
> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Daniel Meneses

### Assignment 5 Requirements:

*Four Parts:*

1. Questions
2. Expanding upon the previous data found in Assignment 4 to extend the model's functionality
3. Creating an Entity Relationship Diagram (ERD) within MS SQL Server to display added tables
4. Create stored procedures and triggers to generate specific reports

#### README.md file should include the following items:

* Screenshot of A5 ERD from MS SQL Server
* Screenshot of SQL code used to generate one of the reports
* Main Bitbucket repo link

#### Assignment Screenshots:

*Screenshot of A5 ERD*:

![A5 ERD Screenshot](img/a5_erd.png)

*Screenshot of Extra Credit Report*:

### Description: Create and display the results of a stored procedure (order_line_total) that calculates the total price for each order line, based upon the product price times quantity, which yields a subtotal (oln_price), total column includes 6% sales tax. Query result set should display order line id, product id, name, description, price, order line quantity, subtotal (oln_price), and total with 6% sales tax. Sort by product ID.

![A5 Extra Credit Report Screenshot](img/a5_ec_report.png)

![A5 Extra Credit Report SQL Code Screenshot](img/a5_ec_report_code.png)

*Screen Recording of A5 SQL code*:

![A5 SQL Code Screen Recording](img/lis3781_a5_sql_code.gif)

*Bitbucket Repo Link*:

[Main LIS3781 Repo Link](https://bitbucket.org/menacious/lis3781/src/master/)
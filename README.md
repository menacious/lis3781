> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 - Advanced Database Management

## Daniel Meneses

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install MySQL
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create tables using MySQL via Terminal
    - Use MySQL to create new users and grant specific permissions
    - Provide screenshots of populated tables

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Access Oracle SQL Developer via RemoteLabs
    - Create and populate tables using the Oracle SQL environment
    - Provide screenshots of populated tables and SQL code

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Performing data obfuscation (i.e. salting and hashing) using MS SQL Server
    - Create Entity Relationship Diagram (ERD) within MS SQL Server environment
    - Provide screenshots of SQL code and diagram

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Expanding upon the previous data found in Assignment 4 to extend the model's functionality
    - Creating an Entity Relationship Diagram (ERD) within MS SQL Server to display added tables
    - Create stored procedures and triggers to generate specific reports

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Use SHA2 Hashing for data obfuscation on MySQL
    - Create stored procedure to give each person a unique randomized salt, and hashed and salted randomized SSN
    - Provide screenshots of populated tables and ERD

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - TBD

**Tables:** Add table: use three or more hyphens (---) to create each column's header, and use pipes (|) to separate each column.  
(Optionally add pipes on either end of the table for compatibility)

| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |

**Alignment:** Align text: left, right, or center by adding a colon (:) to left, right, or on both sides of hyphens within the header row.

| Syntax      | Description | Test Text     |
| :---        |    :----:   |          ---: |
| Header      | Title       | Here's this   |
| Paragraph   | Text        | And more      |

[Markdown Tables](https://www.markdownguide.org/extended-syntax/ "Markdown Tables link")
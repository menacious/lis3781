> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Daniel A. Meneses

### Project 1 Requirements:

*Three Parts:*

1. Questions
2. Using SHA2 Hashing for data obfuscation
3. Create stored procedure to give each person a unique randomized salt, and hashed and salted randomized SSN
4. Create an ERD that illustrates the relationship between all tables

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of populated tables
* Main Bitbucket repo link

#### Assignment Screenshots:

*Screenshot of P1 ERD*:

![P1 ERD Screenshot](img/p1_erd.png)

*Screenshot of P1 main table (person)*:

[![P1 Person Table Screenshot](img/p1_table.png)](img/p1_table.png)

*Assignment Links*:

[LIS3781 Bitbucket Link](https://bitbucket.org/menacious/lis3781/src/master/)